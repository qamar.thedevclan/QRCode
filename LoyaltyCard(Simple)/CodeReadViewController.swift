

import AVFoundation
import UIKit
import QRCodeReader


protocol CodeViewControllerDelegate {
    func didGetQRResult(codeString: String)
}

class CodeReadViewController: UIViewController {
  @IBOutlet var previewView: QRCodeReaderViewCustom!
    
    
    
  lazy var reader: QRCodeReader = QRCodeReader()
    var delegate: CodeViewControllerDelegate?
    
    @IBOutlet weak var torchButton: UIButton!
    @IBOutlet weak var camSwitchButton: UIButton!
    
  // MARK: - Actions

    @IBAction func didTapCloseButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        torchButton.addTarget(self, action: #selector(didTapTorchButton), for: .touchUpInside)
        camSwitchButton.addTarget(self, action: #selector(didTapCamSwitchButton), for: .touchUpInside)
        scanInPreviewAction(self)
    }
    
    @objc func didTapCamSwitchButton() {
        _ = self.reader.switchDeviceInput()
    }
    @objc func didTapTorchButton() {
        self.reader.toggleTorch()
    }

        
  private func checkScanPermissions() -> Bool {
    do {
      return try QRCodeReader.supportsMetadataObjectTypes()
    } catch let error as NSError {
      let alert: UIAlertController

      switch error.code {
      case -11852:
        alert = UIAlertController(title: "Error", message: "This app is not authorized to use Back Camera.", preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Setting", style: .default, handler: { (_) in
          DispatchQueue.main.async {
            if let settingsURL = URL(string: UIApplicationOpenSettingsURLString) {
              UIApplication.shared.openURL(settingsURL)
            }
          }
        }))

        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
      default:
        alert = UIAlertController(title: "Error", message: "Reader not supported by the current device", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
      }

      present(alert, animated: true, completion: nil)

      return false
    }
  }

  @IBAction func scanInPreviewAction(_ sender: Any) {
    guard checkScanPermissions(), !reader.isRunning else { return }

    previewView.setupComponents(showCancelButton: false, showSwitchCameraButton: false, showTorchButton: false, showOverlayView: true, reader: reader)

    reader.startScanning()
    reader.didFindCode = { result in
        if let del = self.delegate {
            del.didGetQRResult(codeString: result.value)
        }
    }
  }

}
